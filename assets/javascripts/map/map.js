angular.module('HackTravelApplication.Map', [])
.controller('MapCtrl', ['$scope', function($scope){
    $scope.map = { 
        center: { 
            latitude: 45.563843, 
            longitude: 12.428101 }, 
        zoom: 13,
        bounds: {}
        
    };

    var styles = [
    {
        stylers: [
          { hue: "#1F6BB4" },
          { saturation: -10 }
        ]
      },{
        featureType: "road.local",
        elementType: "geometry",
        stylers: [
          { lightness: 100 },
          { visibility: "on" }
        ]
      },{
        featureType: "road.local",
        elementType: "labels",
        stylers: [
          { visibility: "simplified" }
        ]
      },{
        featureType: "poi.business",
        elementType: "labels",
        stylers: [
          { visibility: "on" }
        ]
      }
    ];

    $scope.options = {styles: styles};

    var createRandomMarker = function(i, bounds, idKey) {
      var lat_min = bounds.southwest.latitude,
        lat_range = bounds.northeast.latitude - lat_min,
        lng_min = bounds.southwest.longitude,
        lng_range = bounds.northeast.longitude - lng_min;
      if (idKey == null) {
        idKey = "id";
      }

      var latitude = lat_min + (Math.random() * lat_range);
      var longitude = lng_min + (Math.random() * lng_range);
      var ret = {
        latitude: latitude,
        longitude: longitude,
        title: 'm' + i
      };
      ret[idKey] = i;
      return ret;
    };

    $scope.locations = [];

    $scope.$watch(function() {
      return $scope.map.bounds;
    }, function(nv, ov) {
      if (!ov.southwest && nv.southwest) {
        var markers = [];
        for (var i = 0; i < 50; i++) {
          markers.push(createRandomMarker(i, $scope.map.bounds))
        }
        $scope.locations = markers;
      }
    }, true);

    $scope.updatePosition = function(){
        console.log($scope.map.center)

        // var markers = [];
        // for (var i = 0; i < 50; i++) {
        //   markers.push(createRandomMarker(i, $scope.map.bounds))
        // }
        // $scope.locations = markers;
    }
}])