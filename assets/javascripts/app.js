angular.module(
  'HackTravelApplication',
  [
    'ui.router',
    'uiGmapgoogle-maps',
    'HackTravelApplication.Map'
  ])
  .config(function($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise("/map");

  $stateProvider
    .state('map', {
      url: "/map",
      templateUrl: "assets/templates/map.html"
    })
    .state('info', {
      url: "/info",
      templateUrl: "assets/templates/info.html",
    })
});






// function map() {
//   if (typeof google === 'undefined' && !$('#map').length) return false;

//   var LatLng = new google.maps.LatLng(45.563843, 12.428101)
//   var image = "<%= asset_path('responsa-marker.png') %>";
//   var contentString = "<div id='content'>"+
//     "<div id='siteNotice'>"+
//     "</div>"+
//     "<h1><strong>Responsa Srl</strong></h1>"+
//     "<div id='bodyContent'>"+
//     "<p>Tenuta Ca' Tron <br />"+
//     "Via Sile, 41 - 31056 Roncade <br />"+
//     "Treviso - Italy</p>"+
//     "</div>"+
//     "</div>";

//   var map = new google.maps.Map(document.getElementById('map'), {
//     zoom: 14,
//     center: LatLng,
//     disableDefaultUI: true
//   });

//   var infowindow = new google.maps.InfoWindow({
//     content: contentString
//   });

//   var marker = new google.maps.Marker({
//     position: LatLng,
//     map: map,
//     title:"Responsa",
//     icon: image
//   });

//   google.maps.event.addListener(marker, 'click', function() {
//     infowindow.open(map,marker);
//   });

//   marker.setMap(map);
// }